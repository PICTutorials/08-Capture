;   Capture - Capture a rising voltage edge of a PIC pin
;   Copyright (C) 2009  Steven Rodriguez
;
;   This program is part of Capture
;
;   This program is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License
;   along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;   If this program has problems please contact me at:
;
;   stevencrc@digitecnology.zapto.org

;==============================================
;Capture (2009 - Steven Rodriguez)
;==============================================

;General description
;=============================
; The display leds are connected to:
; - RB0 - 6 - bit 0 of the timer1 when captured a signal
; - RB1 - 7 - bit 1 of the timer1 when captured a signal
; - RB2 - 8 - bit 2 of the timer1 when captured a signal
; - RA3 - 2 - bit 3 of the timer1 when captured a signal
; - RB4 - 10 - bit 4 of the timer1 when captured a signal
; - RB5 - 11 - bit 5 of the timer1 when captured a signal
; - RB6 - 12 - bit 6 of the timer1 when captured a signal
; - RB7 - 13 - bit 7 of the timer1 when captured a signal
; - RA2 - 17 - LED to indicate if CCPR1H is showed
; - RA1 - 18 - Switch to change to TMR1L and TMR1H values
; - RB3 - 9 - Switch to send a pulse to PIC
;=============================

;Variables
;==============================
TEMP EQU 0x27

;Assembler directives
;==============================
	list p=16f628a
        include p16f628a.inc
        __config b'11111100111000'
        org 0x00 ;Reset vector
        goto Start		

;Functions
;==============================
	include digitecnology.inc
ShowCCPR1H
	call chgbnk0
	movf PORTB,0
	andlw b'00001000'
	movwf TEMP
	movf CCPR1H,0
	andlw b'11110111'
	iorwf TEMP,0
	movwf PORTB
	btfss CCPR1H,3
	goto $+3
	bsf PORTA,3
	goto $+2
	bcf PORTA,3
	bsf PORTA,2
	return
ShowCCPR1L
	call chgbnk0
	movf PORTB,0
        andlw b'00001000'
	movwf TEMP
	movf CCPR1L,0
	andlw b'11110111'
        iorwf TEMP,0
        movwf PORTB
        btfss CCPR1L,3
	goto $+3
        bsf PORTA,3
	goto $+2
        bcf PORTA,3
	bcf PORTA,2
	return
;Program
;==============================
Start ;Main program
	goto Initialize
Initialize
	;Change to bank 0
	call chgbnk0
	;Clear PORTA and PORTB
	clrf PORTA
	clrf PORTB
	;Change to bank 1
	call chgbnk1
	;Set inputs/outputs
	bcf TRISB,0 ;Output
	bcf TRISB,1 ;Output
	bcf TRISB,2 ;Output
	bsf TRISB,3 ;Input
	bcf TRISB,4 ;Output
	bcf TRISB,5 ;Output
	bcf TRISB,6 ;Output
	bcf TRISB,7 ;Output
	bcf TRISA,2 ;Output
	bsf TRISA,1 ;Input
	bcf TRISA,3 ;Output
	;Deactivate analog comparators
	call chgbnk0
	bsf CMCON,2
	bsf CMCON,1
	bsf CMCON,0	
	;Configure TIMER1
 	bcf T1CON,5 ;Set 1:1 prescale value
	bcf T1CON,4
	bcf T1CON,3 ;Oscillator disables
	bcf T1CON,1 ;Use internal oscillator
	bsf T1CON,0 ;Enables TIMER1
	;Configure capture mode
	bcf CCP1CON,3 ;Capture when a rising edge comes
	bsf CCP1CON,2
	bcf CCP1CON,1
	bsf CCP1CON,0	
Cycle		
	;Check input
	btfss PORTA,1
	goto $+3
	call ShowCCPR1H
	goto $+2
	call ShowCCPR1L
	goto Cycle
	end
